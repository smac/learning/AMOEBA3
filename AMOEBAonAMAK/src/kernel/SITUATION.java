package kernel;

import java.io.Serializable;

public enum SITUATION implements Serializable {
	
	LEARN,
	REQUEST,
	CONCURENCE,
	INCOMPETENCE
	
}
