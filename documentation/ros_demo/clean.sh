#!/bin/sh

rm amoeba.jar
rm ./*.log
rm -R ./saves/
rm -R ./catkin_ws/build/
rm -R ./catkin_ws/devel/
rm -R ./catkin_ws/install/
