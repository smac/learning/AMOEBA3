# What is ...
## AMAKFX :
AMAKFX is a fork of [AMAK](https://bitbucket.org/perlesa/amak/src/master/) (A Multi Agent frameworK), modified to use JavaFX instead of Swing for the user interface.
It also provide various improvements or modifications allowing for easier use with AMOEBA.

## AMOEBA :
AMOEBA (Agnostic MOdElBuilder by self-Adaptation) is a Multi-Agent system based on the Adaptative Multi Agent System (AMAS) approach, designed for building model of complex Systems at real time. Real time meaning it adapt quickly. 
For more information on AMOEBA, see [Self-Adaptive Model Generation for Ambient Systems](https://www.sciencedirect.com/science/article/pii/S1877050916301818).

## AMOEBAonAMAK :
AMOEBAonAMAK, as its name implies, is a port of AMOEBA onto AMAKFX, allowing AMOEBA's developer and user to focus on agent's behaviour, and not nitty-gritty details like, scheduling, multithreading, and GUI. 